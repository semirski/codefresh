#Codefresh Devops exam
##This repository contains code that spins up vagrant ubuntu box and provisions it with kubernetes cluster and local volume provisioner.

##1. Prerequisites.
###To be able to run the code the next software should be installed on desktop:
* [ Vagrant ](https://www.vagrantup.com/downloads.html)
* [ Virtualbox ](https://www.virtualbox.org/wiki/Downloads)
* [ Ansible ](http://docs.ansible.com/ansible/latest/intro_installation.html#installing-the-control-machine)
* [ vagrant-vbguest plugin ](https://github.com/dotless-de/vagrant-vbguest)
##2. Running the code

```
git clone codefresh
cd codefresh
vagrant up
```

There are two things that ##MUST be changed before provisioning:

- ansible_ssh_private_key in `inventory` file should refer to existing ssh private key 
- k8s_config .ssh.password in `Vagrant` file should be matched the password generated by vagrant on your system

That's because vagrant box first run will be unsuccessful.

Type `vagrant ssh-config` to find the correct path to ssh private ket for ansible_ssh_private_key option.

The password generated by vagrant can be found using `cat ~/.vagrant.d/boxes/ubuntu-VAGRANTSLASH-xenial64/20170830.1.1/virtualbox/Vagrantfile | grep password`. The path can be not exact but similar.
